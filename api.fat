{ random, floor } = { _ <- fat.math }
{ readBin } = { _ <- fat.file }
_ <- fat.type.Text
_ <- fat.type.List
_ <- fat.console
_ <- db

db = fazer_db("db.json")

chars_validos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
quant_chars_validos = chars_validos.size()

gerar_id = (len: Number) -> {
    ~ base = []
    1..len @ -> {
        index_aleatório = floor(random() * quant_chars_validos)
        base += [ chars_validos(index_aleatório) ]
    }

    base.join('')
}

parse_parametros_body = (txt: Text) -> {
    lista_relacoes = txt.isEmpty | !txt.contains("=") 
        ? {} 
        : txt.split("&")

    escopo_parametos = {}
    lista_relacoes @ relacao -> {
        pos_split = relacao.indexOf("=")
        chave = relacao(0, pos_split - 1)
        valor = relacao(pos_split + 1, -1)
        escopo_parametos.[chave] = valor
    }
    escopo_parametos
}

pegar_host = (headers: List/Text): Text -> {
    comeco = 'Host: '
    linha_host = headers.find(-> _.startsWith(comeco))

    !linha_host => null
    _ => linha_host(comeco.size..)
}

criar_link = (r: HttpRequest): HttpResponse -> {
    escopo_parametos = parse_parametros_body(r.body.toText)
    host = pegar_host(r.headers)
    
    !escopo_parametos.destino | !host => HttpResponse(status = 400)
    _ => {
        slug = escopo_parametos.slug.isEmpty 
            ? gerar_id(7) 
            : escopo_parametos.slug

        destino_unencoded = escopo_parametos.destino.replace("%3A", ":").replace("%2F", "/")
        db.set(slug, destino_unencoded)
        
        link_final = '{host}/r/{slug}'
        html_criado = file.read("static/criado.html").replace("{{link}}", link_final)

        HttpResponse(body = html_criado, headers = [ "text/html" ])
    }
}

redirecionar = (r: HttpRequest): HttpResponse -> {
    chave = r.path.split('/')(-1)
    linkTalvez = db.get(chave)
    linkTalvez ? {
        headerzinho = ['Location: {linkTalvez}']
        HttpResponse(status = 301, headers = headerzinho)
    } : {
        HttpResponse(status = 404)
    }
}
