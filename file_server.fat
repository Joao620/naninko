_ <- fat.type.Text
http <- fat.http
file <- fat.file

FileServer = {
    caminho_raiz: Text

    at = (request: HttpRequest): HttpResponse -> {
        path = self.caminho_raiz + request.path
        type = self.getContentType(path)

        request.path.endsWith('/') & file.exists(path + 'index.html') 
                          => HttpResponse(body = file.readBin(path + 'index.html'), 
                                headers = [ self.getContentType('.html') ])
        !type             => HttpResponse(status = 403)  # forbidden
        file.exists(path) => HttpResponse(body = file.readBin(path), headers = [ type ])
        _                 => HttpResponse(status = 404)  # not found
    }

    mime_types = {
        html = "text/html"
        fat = "text/awesome-language"
        htm = "text/html"
        js = "application/javascript"
        json = "application/json"
        css = "text/css"
        md = "text/markdown"
        xml = "application/xml"
        csv = "text/csv"
        txt = "text/plain"
        svg = "image/svg+xml"
        rss = "application/rss+xml"
        atom = "application/atom+xml"
        png = "image/png"
        jpg = "image/jpg"
        jpeg = "image/jpeg"
        gif = "image/gif"
        ico = "image/icon"
        ttf = "text/css"
    }

    getContentType = (path: Text): Text -> { 
        tipo_no_path = path.split('.')(-1)
        tipo = self.mime_types(tipo_no_path)
        tipo ? 'Content-Type: ' + tipo : null
    }
}
